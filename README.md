# PS-EXCH-New-Hire

To be run against all new hires.

## Description

This script will perform the following on each account it is run against:

* Adds Reviewer rights to the specified Admin account
* Disables OWA
* Adds to DenyAll Client Access Rule

### Version History

- 1.4 - 03/25/19
    - Added -Confirm:$false to CAR
- 1.3 - 03/01/19
    - Added Repo link to NOTES section
- 1.2 - 03/01/19
    - Updated DESCRIPTION section
- 1.1 - 03/01/19
    - Added version number
    - Adjusgted wording of prompts
- 1.0 - 03/01/19
    - Initial Upload