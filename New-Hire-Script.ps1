<#  
.SYNOPSIS  
    New Hire Script  
.DESCRIPTION  
    Should be run for each new hire Exchange account.  Performs the following on each account:
    1. Adds Reviewer rights to the specified Admin account
    2. Disables OWA
    3. Adds to DenyAll Client Access Rule
.NOTES  
    File Name       : New-Hire-Script.ps1
    Version         : 1.4
    Author          : BJ Beier - https://gitlab.com/bjbeier/ps-exch-new-hire
#>

# Get domain name

    Write-Host "`n"
    $Domain = Read-Host -Prompt 'Enter domain name with @ symbol'

# Get admin/group name

    Write-Host "`n"
    $Admin = Read-Host -Prompt 'Enter the Admin/Group you would like to grant reviewer permissions for - Only username, not full email'

# Get username
        
    Write-Host "`n"
    $UserID = Read-Host -Prompt 'Enter new hire mailbox - Only username, not full email'

# Set Variables

    $AdminAccount = $Admin+=$Domain
    $Access = "Reviewer"
    $Exclusions = @("/Sync Issues",
                    "/Sync Issues/Conflicts",
                    "/Sync Issues/Local Failures",
                    "/Sync Issues/Server Failures",
                    "/Recoverable Items",
                    "/Deletions",
                    "/Purges",
                    "/Versions"
                    )
    
# Create mailbox variable
    
    $Mailbox = $UserID+=$Domain
     
# Write notification to screen
    
    Write-Host "`n Commands will be run against $Mailbox"
    
# Get Mailbox Folders for account
     
    $Mailboxfolders = @(Get-MailboxFolderStatistics $Mailbox | Where-Object {!($Exclusions -icontains $_.FolderPath)} | Select-Object FolderPath)
     
# Write notification to screen
     
    Write-Host "`n Adding $AdminAccount to $Mailbox with $Access permissions"
     
# 1. Apply reviewer access to all folders
     
    foreach ($Mailboxfolder in $Mailboxfolders)
    {
        $Folder = $Mailboxfolder.FolderPath.Replace("/","\")
        if ($folder -match "Top of Information Store")
        {
           $Folder = $Folder.Replace("Top of Information Store","")
        }
        $Identity = "$($Mailbox):$folder"
        try
        {
            Add-MailboxFolderPermission -Identity $Identity -User $AdminAccount -AccessRights $Access -ErrorAction SilentlyContinue
        }
        catch
        {
            Write-Warning $_.Exception.Message
        }
    }
     
# 2. Disable OWA
    
    Write-Host "`n Disabling OWA for $Mailbox"
     
    Set-CasMailbox -Identity $Mailbox -OWAEnabled $false

# 3. Add To DenyAll Client Access Rule

    Write-Host "`n Adding $Mailbox to DenyAll Client Access Rule"

    Set-ClientAccessRule "DenyAll" -UsernameMatchesAnyOfPatterns @{Add="$Domain\$Mailbox"} -Confirm:$false